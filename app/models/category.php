<?php

class Category extends Model {
  const table = 'category';

  public function category() {
    parent::__construct();
  }

  public function insertCategoryRecord() {
    try {
      $query = $this->db->prepare('INSERT IGNORE INTO '.self::table.' (parent, child) VALUES (:parent, :child)');
      $result = $query->execute(array(':parent' => $this->parent, ':child' => $this->child));
      return $result;
    }
    catch (PDOException $ex) {
      if ($ex->getCode() != 23000) throw $ex;
    }
  }

  public function checkParentValue($value) {
    $query = $this->db->prepare('SELECT parent FROM '.self::table.' WHERE parent = :value or child = :value');
    $query->execute(array(':value' => $value));
    $data = $query->rowCount();
    return $data;
  }

/*
SELECT CONCAT( REPEAT(' ', COUNT(node_parent.parent) - 1), node.child) AS name
FROM category AS node,
        category AS node_parent
WHERE node.lft BETWEEN node_parent.parent AND node_parent.child
GROUP BY node.name
ORDER BY node.lft
 */

  public function displayCategoryTree() {
    $query = $this->db->prepare('SELECT parent, child FROM '.self::table.' order by child');
    $query->execute();
    $data = $query->fetchAll();
    return $data;
  }

}
