<?php

/**
 *
 */
class Home extends Controller {

	public function index($name = '') {
		$category = $this->model('category');

    if (!empty($_POST['category'])) {
      $category->value = $_POST['category'];
      $data['submission_status'] = $this->processSubmittedCategory($category);
    } elseif (isset($_POST['add']) && empty($_POST['category'])) {
      $data['submission_status'] = 'You need to type a value greater than 0 into the category field.';
    }
    $data['displayCategoryTree'] = $category->displayCategoryTree();

    $this->view('home/index', $data);
	}

  public function processSubmittedCategory($category) {
    list($valid, $message) = $this->validateInput($category->value);
    if ($valid == 1) {
      // 1. ensure parent value exists
      $countPeriods = substr_count($category->value, '.');
      if ($countPeriods > 0) {
        $checkValue = $this->remapArrayValues($category->value, $countPeriods);
        $parentExists = $category->checkParentValue($checkValue);
        if ($parentExists == 0) return "Please input a parent category for $checkValue";
      }

      // process submission here
      // 2. get parent.child
      $record = $this->parseParentChildValues($category);

      // 3. insert the record
      $category->parent = $record['parent'];
      $category->child = $record['child'];
      $result = $category->insertCategoryRecord();
      if ($result == 1) {
        return "Your category $category->value was inserted.";
      } else {
        return "Something went wrong with $category->value.";
      }
    } else {
      return $message;
    }
  }

  public function validateInput($value) {
    if (!preg_match('/^[1-9.]+$/', $value)) return array(0, 'You may only enter numbers starting with greater than 0 and a period.'); // ensure $value only contains numbers and periods
    if (!preg_match('/^\d/', $value)) return array(0, 'Your input must start with a number.'); // ensure $value begins with a number
    if (!is_numeric(substr($value, -1, 1))) return array(0, 'Your input must end with a number.'); // ensure $value ends with a number
    if (stripos($value, '..')) return array(0, 'Your input may not contain consecutive periods.'); // ensure $value doesn't contain consecutive periods
    return array(1, '');
  }

  public function parseParentChildValues($category) {
    $countPeriods = substr_count($category->value, '.');
    if ($countPeriods == 0) {
      return array('parent' => $category->value, 'child' => '');
    } else {
      $parent = $this->remapArrayValues($category->value, $countPeriods);
      return array('parent' => $parent, 'child' => $category->value);
    }
  }

  public function remapArrayValues($value, $position) {
    // echo "position: $position <br />";
    $values = explode('.', $value);
    unset($values[$position]);
    return implode('.', $values);
  }

}
