--
-- Database: `aso_exam`
--
CREATE DATABASE IF NOT EXISTS `aso_exam` DEFAULT CHARACTER SET latin1 COLLATE latin1_swedish_ci;
USE `aso_exam`;

-- --------------------------------------------------------

--
-- Table structure for table `category`
--

CREATE TABLE IF NOT EXISTS `category` (
  `parent` varchar(500) NOT NULL,
  `child` varchar(500) NOT NULL DEFAULT '',
  PRIMARY KEY (`parent`,`child`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
